
Actuator also integrates with external application monitoring systems like Prometheus, Graphite, DataDog, Influx, Wavefront, New Relic and many more. 
These systems provide you with excellent dashboards, graphs, analytics, and alarms to help you monitor and manage your application from one unified interface.
Actuator uses Micrometer, an application metrics facade to integrate with these external application monitoring systems. This makes it super easy to plug-in 
any application monitoring system with very little configuration.


Prometheus is an open-source monitoring system that was originally built by SoundCloud. It consists of the following core components -

A data scraper that pulls metrics data over HTTP periodically at a configured interval.

A time-series database to store all the metrics data.

A simple user interface where you can visualize, query, and monitor all the metrics.

Grafana

Grafana allows you to bring data from various data sources like Elasticsearch, Prometheus, Graphite, InfluxDB etc, and visualize them with beautiful graphs.

It also lets you set alert rules based on your metrics data. When an alert changes state, it can notify you over email, slack, or various other channels.

Note that, Prometheus dashboard also has simple graphs. But Grafana’s graphs are way better. That’s why, in this post, we’ll integrate Grafana with Prometheus to import and visualize our metrics data


Adding Micrometer Prometheus Registry to your Spring Boot application

Spring Boot uses Micrometer, an application metrics facade to integrate actuator metrics with external monitoring systems.

It supports several monitoring systems like Netflix Atlas, AWS Cloudwatch, Datadog, InfluxData, SignalFx, Graphite, Wavefront, Prometheus etc.

To integrate actuator with Prometheus, you need to add the micrometer-registry-prometheus dependency -

    compile 'io.micrometer:micrometer-registry-prometheus:1.3.5'
    
Once you add the above dependency, Spring Boot will automatically configure a PrometheusMeterRegistry and a CollectorRegistry to collect and export metrics data in a format that can be scraped by a Prometheus server.

All the application metrics data are made available at an actuator endpoint called /prometheus. The Prometheus server can scrape this endpoint to get metrics data periodically.

Downloading and Running Prometheus using Docker

1. Downloading Prometheus

You can download the Prometheus docker image using docker pull command like so -

$ docker pull prom/prometheus


Prometheus Configuration (prometheus.yml)

Next, We need to configure Prometheus to scrape metrics data from Spring Boot Actuator’s /prometheus endpoint.
The above configuration file is an extension of the basic configuration file available in the Prometheus documentation. [https://prometheus.io/docs/prometheus/latest/getting_started/#configuring-prometheus-to-monitor-itself][doc]

The most important stuff to note in the above configuration file is the spring-actuator job inside **scrape_configs** section.

The **metrics_path** is the path of the **Actuator’s prometheus endpoint**. The targets section contains the HOST and PORT of your Spring Boot application.

Please make sure to replace the HOST_IP with the IP address of the machine where your Spring Boot application is running.

ipconfig getifaddr en0 for mac users to get IP adress

 Note that, localhost won’t work here because we’ll be connecting to the HOST machine from the docker container. You must specify the network IP address.
 
 Running Prometheus using Docker
 
 Finally, Let’s run Prometheus using Docker. Type the following command to start a Prometheus server in the background -
 
 docker run -d --name=prometheus -p 9090:9090 -v <PATH_TO_prometheus.yml_FILE>:/etc/prometheus/prometheus.yml prom/prometheus --config.file=/etc/prometheus/prometheus.yml


Please make sure to replace the <PATH_TO_prometheus.yml_FILE> with the PATH where you have stored the Prometheus configuration file.

After running the above command, docker will start the Prometheus server inside a container. You can see the list of all the containers with the following command -
docker ps -a 

Visualizing Spring Boot Metrics from Prometheus dashboard

That’s it! You can now navigate to http://localhost:9090 to explore the Prometheus dashboard.

You can enter a Prometheus query expression inside the Expression text field and visualize all the metrics for that query.

Following are some Prometheus graphs for our Spring Boot application’s metrics -

System’s CPU usage -

for more expressins : https://prometheus.io/docs/introduction/first_steps/#using-the-expression-browser


Downloading and running Grafana using Docker

Type the following command to download and run Grafana using Docker -

$ docker run -d --name=grafana -p 3000:3000 grafana/grafana 

The above command will start Grafana inside a Docker container and make it available on port 3000 on the Host machine.

You can type docker container ls to see the list of Docker containers -

That’s it! You can now navigate to http://localhost:3000 and log in to Grafana with the default username admin and password admin.

Configuring Grafana to import metrics data from Prometheus

Follow the steps below to import metrics from Prometheus and visualize them on Grafana:

1. Add the Prometheus data source in Grafana
n the login page, type admin for the username and password.
Change your password.

Add data source -> Prometheus-> http-> url : http://HOST_IP:9090

2. Create a new Dashboard with a Graph


[doc]: https://prometheus.io/docs/prometheus/latest/getting_started/#configuring-prometheus-to-monitor-itself