package fr.deeplearning.prometheus.controller;

import fr.deeplearning.prometheus.domain.Story;
import fr.deeplearning.prometheus.repository.StoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StroryController {

    @Autowired
    private StoryRepository storyRepository;

    @GetMapping("/stories")
    public List<Story> getAllStories() {
        return storyRepository.findAll();
    }
}
