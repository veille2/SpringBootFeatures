
1. Create a file '**banner.txt**' under the ressources folder.

2. Generate a banner :

You can add a text to your '**banner.txt**' file or use a website to generate a beautiful banner.
In this example, I used : [this web site]

3. run your application.

##### Banner Options :

1. There are 3 Banner.Mode :

   - **OFF**       To disable the banner.
   - **CONSOLE**   To print the banner to System.out.
   - **LOG**       To print the banner into log file.

2. There are 3 ways to modify the Spring logo banner  

_2.1. Modify the Application main method :_ 

`new SpringApplicationBuilder(Application.class)
                .bannerMode(Banner.Mode.OFF)  //to disable for example 
                .run(args);`
                
                
_2.2. Modify the application.properties/application.yml_

`spring:
  main:
    banner-mode:"off"`
    
_2.3. In the terminal, you can pass it as a system property_

`java -Dspring.main.banner-mode=off -jar spring-boot-simple-1.0.jar`


For more options you can visit : https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/htmlsingle/#boot-features-banner

[this web site]: https://devops.datenkollektiv.de/banner.txt/index.html