package fr.deeplearning.logging;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@Slf4j
@SpringBootApplication()
public class Application {

    public static void main(String[] args) {

        new SpringApplicationBuilder(Application.class)
                .run(args);
        log.debug("Debugging log");
        log.info("Info log");
        log.warn("Warning log");
        log.error("Error log");
    }
}
