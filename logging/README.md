
### Integrate Log4j2 in Spring Boot applications

Log4j 2 provides a significant improvement compared to its predecessor which includes:. 
- Async Loggers.
- Java 8-style lambda support for lazy logging.
- Supported APIs: SLF4J, Commons Logging, Log4j-1.x and java.util.logging.
- Automatically reload its configuration.
- Custom log levels.


##### Configuring Log4j2:

All the Spring Boot starters depend on spring-boot-starter-logging, which uses Logback by default. 
To use Log4j2, we need to exclude spring-boot-starter-logging and add spring-boot-starter-log4j2 dependency.

     compile('org.springframework.boot:spring-boot-starter-log4j2:2.1.2.RELEASE') {
         exclude 'org.springframework.boot:spring-boot-starter-logging:2.2.4.RELEASE'
     }

There are 4 ways to configure Log4j2 for an application:

- XML (log4j2.xml) based configuration (default configuration)
- JSON (log4j2.json)
- YAML( log4j2.yaml)
- Properties file

Spring Boot automatically configures Log4j if it finds one of those files name in the classpath.

###### Adding a Rolling File Appender:

The logs can be written to a file using  RollingFile appender. 
RollingFile appender creates a new file whenever the log file reaches a certain threshold specified by the triggering policy.
It stores the old log files with names matching the pattern specified by the filePattern parameter 


###### Adding an SMTP Appender

SMTP appender is very useful in production systems when we need to be notified about any errors in our application via email.

However, we need to include spring-boot-starter-mail dependency because SMTP appender requires an SMTP server 

    compile 'org.springframework.boot:spring-boot-starter-mail:2.2.2.RELEASE'

###### Asynchronous Logging

Async loggers provide a drastic improvement in performance compared to their synchronous counterparts. To use this king of appender 
we need to include Distruptor library in our gradle.
    
    compile 'com.lmax:disruptor:3.4.0'
