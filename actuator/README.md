#### Spring Boot Actuator

Spring Boot Actuator module provides production-ready features as health check-up, auditing, metrics gathering, HTTP tracing etc
to monitor and manage a Spring Boot application once pushed in production. Actuator creates several so-called endpoints that can be exposed over HTTP or JMX 
to let monitoring and interacting with the application.

To enable production ready feature in a gradle based project, we need to add the following ‘Starter’ dependency:
 
 dependencies {
                 compile("org.springframework.boot:spring-boot-starter-actuator")
               }
Oncee the application is started, connect to : http://localhost:8080/actuator. By default, only the health and info endpoints are exposed over HTTP.

 - /health : provides basic information about the application’s health. 

The status will be UP as long as the application is healthy. It will show DOWN if the application gets unhealthy due to any issue 
like connectivity with the database or lack of disk space etc.

- /info :  displays general information about the application obtained from build files like META-INF/build-info.properties or Git files like git.properties or through any environment property under the key info.

- /metrics : shows several useful metrics information like JVM memory used, system CPU usage, open files, and much more.
- /loggers : shows application’s logs and also lets you change the log level at runtime.

The complete list of the actuator endpoints is [here]


### Enabling and Disabling Actuator Endpoints


Every actuator endpoint can be explicitly enabled and disabled  only by setting the property 
management.endpoint.<id>.enabled to true or false (where id is the identifier for the endpoint).

             `management.endpoint.shutdown.enabled=true `

By default, all endpoints except for shutdown are enabled

Moreover, the endpoints also need to be exposed over HTTP or JMX to make them remotely accessible.Some Endpoints may contain sensitive information,

### Exposing Actuator Endpoints

By default, all the actuator endpoints are exposed over JMX but only the health and info endpoints are exposed over HTTP.

## Exposing Actuator endpoints over HTTP

# Use "*" to expose all endpoints, or a comma-separated list to expose selected ones
management.endpoints.web.exposure.include=health,info 
management.endpoints.web.exposure.exclude=

## Exposing Actuator endpoints over JMX

# Use "*" to expose all endpoints, or a comma-separated list to expose selected ones
management.endpoints.jmx.exposure.include=*
management.endpoints.jmx.exposure.exclude=


### Displaying detailed health information

The health endpoint only shows a simple UP or DOWN status. To get the complete details including the status of every health indicator that was checked as part of the health check-up process,
add the following property in the application.properties file

management.endpoint.health.show-details=always

If your application has a database (say MySQL), the health endpoint will show the status of that as well 

[here]: https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-features.html#production-ready-endpoints