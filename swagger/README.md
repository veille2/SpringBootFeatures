
Swagger specifications are implemented by Springfox suit of java libraries.

Nowadays, front-end and back-end components often separate a web application. Usually, we expose APIs as a back-end component for the front-end component or third-party app integrations.

In such a scenario, it is essential to have proper specifications for the back-end APIs. At the same time, the API documentation should be informative, readable, and easy to follow.

The configuration of Swagger mainly centers around the Docket bean.

Swagger 2 is enabled through the @EnableSwagger2 annotation.

After the Docket bean is defined, its select() method returns an instance of ApiSelectorBuilder, which provides a way to control the endpoints exposed by Swagger.

Predicates for selection of RequestHandlers can be configured with the help of RequestHandlerSelectors and PathSelectors. Using any() for both will make documentation for your entire API available through Swagger.

This configuration is enough to integrate Swagger 2 into an existing Spring Boot project. For other Spring projects, some additional tuning is required.


Swagger UI

Swagger UI is a built-in solution which makes user interaction with the Swagger-generated API documentation much easier.

http://localhost:8080/your-app-root/swagger-ui.html  



Bean Validations

Springfox also supports the bean validation annotations through its springfox-bean-validators library.

First, we'll add the Maven dependency to our pom.xml:

	'compile 'io.springfox:springfox-bean-validators:2.4.0'



Redémarrez votre application et rendez-vous sur http://localhost:9090/v2/api-docs. Vous obtenez alors la documentation complète de votre Microservice, générée au format JSON.

Vous pouvez également accéder à une version au format HTML à l'adresse suivante : http://localhost:9090/swagger-ui.html, comme illustré ci-après :



intéressantes pour la personnalisation de Swagger, grâce à la classe Docket qui gère toutes les configurations.

On commence alors par initialiser un objet Docket en précisant que nous souhaitons utiliser swagger 2.
select permet d'initialiser une classe du nom de ApiSelectorBuilder qui donne accès aux méthodes de personnalisation suivantes. Nous vous attardez pas sur cette méthode, elle n'est d'aucune utilité pour la suite.
apis est la première méthode importante. Elle permet de filtrer la documentation à exposer selon les contrôleurs. Ainsi, vous pouvez cacher la documentation d'une partie privée ou interne de votre API. Dans ce cas, nous avons utilisé RequestHandlerSelectors.any().
RequestHandlerSelectors est un prédicat (introduit depuis java 8) qui permet de retourner TRUE ou FALSE selon la conditions utilisée. Dans ce cas, nous avons utilisé any qui retournera toujours TRUE. En d'autres termes, nous indiquons vouloir documenter toutes les classes dans tous les packages. RequestHandlerSelectors offre plusieurs autres méthodes comme annotationPresent qui vous permet de définir une annotation en paramètre. Swagger ne documentera alors que les classes qu'il utilise. La plus utilisée est basePackage qui permet de trier selon le Package. Nous allons voir un exemple juste après.
paths : cette méthode donne accès à une autre façon de filtrer : selon l'URI des requêtes. Ainsi, vous pouvez, par exemple, demander à Swagger de ne documenter que les méthodes qui répondent à des requêtes commençant par "/public" .




Personnalisez la documentation grâce aux annotations
La dépendance Swagger que nous avons utilisée, nous donne accès à un certain nombre d'annotations pour personnaliser la documentation directement dans nos classes.

Nous pouvons ainsi ajouter une description pour chaque API grâce à l'annotation  @Api , comme illustré ci-après :

@Api( description="API pour es opérations CRUD sur les produits.")
@RestController
public class ProductController {
    .....
}
Nous pouvons également définir une description pour chaque opération /méthode à l'aide de l'annotation@ApiOperation  :

    //Récupérer un produit par son Id
    @ApiOperation(value = "Récupère un produit grâce à son ID à condition que celui-ci soit en stock!")
    @GetMapping(value = "/Produits/{id}")
    public Product afficherUnProduit(@PathVariable int id) {
            ......
        }



	compile "io.springfox:springfox-swagger2:2.9.2"
	compile 'io.springfox:springfox-swagger-ui:2.9.2'