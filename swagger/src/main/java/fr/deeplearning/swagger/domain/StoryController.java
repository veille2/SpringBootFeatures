package fr.deeplearning.swagger.domain;

import fr.deeplearning.swagger.repository.StoryRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Api(value = "Exposed story API")
public class StoryController {

    @Autowired
    private StoryRepository storyRepository;

    @GetMapping("/stories")
    @ApiOperation(value = "get all stories")
    public List<Story> getAllStories() {

        return storyRepository.findAll();
    }
}
