package fr.deeplearning.swagger.repository;

import fr.deeplearning.flyway.Application;
import fr.deeplearning.flyway.domain.Story;
import fr.deeplearning.flyway.repository.StoryRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {Application.class})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@Sql(scripts = {"classpath:db/V3__init.sql"})
public class StoryRepositoryTest {

    @Autowired
    private StoryRepository storyRepository;

    @Test
    public void test_find_story_by_id() {
        var story = storyRepository.findById(1);
        assertThat(story).isNotEmpty();
        assertThat(story.get()).usingRecursiveComparison().isEqualTo(Fixtures.STORY);
    }

    private static class Fixtures {

        public static final Story STORY = Story.builder()
                                               .id(1)
                                               .title("Show all terminated stories")
                                               .description("A user can be able to show all finished stories")
                                               .storyPoints(5)
                                               .done(true)
                                               .build();
    }

}
