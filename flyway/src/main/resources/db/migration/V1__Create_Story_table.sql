CREATE TABLE story (
    id          INT(11)      NOT NULL AUTO_INCREMENT,
    title       varchar(100) NOT NULL,
    description varchar(500) NOT NULL,
    done        boolean NOT NULL,
    created_on  date DEFAULT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY title (title)
) ;

