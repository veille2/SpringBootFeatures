

![flyway logo](https://github.com/flyway/flyway-test-extensions/blob/master/image/logo-flyway-test-extensions.png) 


don't forget to store it in my repo (image)


Manage your database with Flyway and Gradle

 automating DB schema migration
Flyway updates a database from one version to a next using migrations. We can write migrations either in SQL with database-specific syntax or in Java for advanced database transformations.


The most current version of Flyway is 4.2.0. Once added in the build.gradle file, Spring Boot will automatically detect the Flyway classes on its classpath and will auto configure it. In Spring Boot, there is a class called FlywayAutoConfiguration in package org.springframework.boot.autoconfigure.flyway being part of the spring-boot-autoconfigure dependency/JAR file which does this job.
Per default, the newly added dependency will auto configure Flyway with the following settings (documented here):https://docs.spring.io/spring-boot/docs/1.5.8.RELEASE/reference/htmlsingle/#common-application-properties



It is important to note that per default, Flyway will be executing migrations against the default data source. If you have more data sources configured, you need to add the @FlywayDataSource annotation as detailed in the Spring Boot documentation.
https://docs.spring.io/spring-boot/docs/current/reference/html/howto.html#howto-database-initialization


As you can see, the only two properties being preconfigured are flyway.baseline-version and flyway.locations. In Flyway the database migrations are specified in SQL files. The default location for those migration files is src/main/resources/db/migration. In this directory you can place files following this naming scheme: V<version number>__<description>.sql. The version number will basically be a positive number which will have to be incremented by the database migration script author with every database migration. flyway.baseline-version configures the initial version number to be 1, so your first database migration file will start with V2__<description>.sql.


This initial creation is called “baselining”. It will create the schema_version table and will assume that the target database is in the most current state, it will not execute any database migration files around found in db/migration. However, baselining is something that we have to trigger, it is not done automatically.

As mentioned above, baseline is such a command that we have to do via the Gradle integration (and therefore a Gradle tasks) or the Flyway command-line client. In our case, we use the Gradle integration:

plugins {
    id "org.flywaydb.flyway" version "4.2.0"
}
Besides activating the Flyway Gradle plugin, we also have to specify the database connection properties in our build.gradle file:

flyway {
    url = 'jdbc:h2:mem:mydb'
    user = 'myUsr'
    password = 'mySecretPwd'
    schemas = ['schema1', 'schema2', 'schema3']
    placeholders = [
        'keyABC': 'valueXYZ',
        'otherplaceholder': 'value123'
    ]
}

More information on configuring Flyway with Gradle can be found here. https://flywaydb.org/documentation/gradle/
Once we have configured our Flyway integration, we can run baseline against our target database. With Gradle, this is done by executing the flywayBaseline task. After it was successfully executed, we will have a new schema_version table in our database that will actually hold only one row of type BASELINE. This is the starting point for database migrations.

Hint: do not forget to switch your hibernate.hbm2ddl.auto setting to none instead of update or any other option which would cause the target database to be modified.



However, as we have turned off Hibernate’s schema migration with hibernate.hbm2ddl.auto=none, we need to add the VERSION column ourselves. First step is to create a new database migration file and place it in db/migration. As it is our first database migration and the baseline version starts with 1 (according to the default config values), we need to call it V2__add_version_to_car.sql. The add_version_to_car can be completely arbitrary text, important is the version number after V. Inside this SQL file, we can use arbitrary statements for our target database. Flyway supports a wide range of databases, a full list can be found here.

So far, we were running with the default settings provided by Spring Boot’s auto configuration mechanism. It is important to note though, that those configuration settings can of course be overridden by the application. For example, the flyway.locations configuration value supports a special variable {vendor} which can be used in the directory location for the specific database type. In our case, the database type is H2 so we could rewrite flyway.locations to

flyway.locations=classpath:db/migration/{vendor}/

Creating migration
We create a first migration called src/main/resources/db/migration/V1__Create_person_table.sql:

Migrating the database
It's now time to execute Flyway to migrate our database:

$ gradle flywayMigrate -i